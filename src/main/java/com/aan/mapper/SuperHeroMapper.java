package com.aan.mapper;

import com.aan.domain.SuperHero;
import com.aan.dto.SuperHeroDTO;
import org.mapstruct.Mapper;

@Mapper
public interface SuperHeroMapper {

    SuperHero toEntity(SuperHeroDTO superHeroDTO);

    SuperHeroDTO toDTO(SuperHero superHero);
}
