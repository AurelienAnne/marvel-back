package com.aan.domain;

public class SuperHero {

    private String name;
    private String thumbnailURL;
    private String description;

    public SuperHero(String name, String thumbnailURL, String description) {
        this.name = name;
        this.thumbnailURL = thumbnailURL;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
