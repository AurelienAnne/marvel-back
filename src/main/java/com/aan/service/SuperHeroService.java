package com.aan.service;

import com.aan.domain.SuperHero;
import com.aan.dto.SuperHeroDTO;
import com.aan.mapper.SuperHeroMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SuperHeroService {

    private static final String URL_MARVEL_CHARACTERS = "https://gateway.marvel.com:443/v1/public/characters";
    private static final Integer LIMIT = 20;

    private final SuperHeroMapper superHeroMapper = Mappers.getMapper(SuperHeroMapper.class);

    public List<SuperHeroDTO> getSuperHeroesPage(Integer pageNumber) {
        // Http Request
        String marvelResponse = this.sendRequest(pageNumber * LIMIT);

        // Parsing
        List<SuperHero> superHeroes = this.parseData(marvelResponse);

        // Mapping
        return superHeroes.stream().map(superHeroMapper::toDTO).collect(Collectors.toList());
    }

    private List<SuperHero> parseData(String response) {
        List<SuperHero> superHeroes = new ArrayList<>();

        Gson gson = new Gson();
        JsonObject jsonObject = gson.fromJson(response, JsonObject.class);
        JsonObject data = jsonObject.getAsJsonObject("data");
        JsonArray characters = data.getAsJsonArray("results");

        for (int i = 0; i < characters.size(); i++) {
            JsonObject hero = characters.get(i).getAsJsonObject();

            String heroName = hero.get("name").getAsString();
            String heroDescription = hero.get("description").getAsString();
            JsonObject thumbnail = hero.get("thumbnail").getAsJsonObject();
            String heroThumbnail = thumbnail.get("path").getAsString() + "." + thumbnail.get("extension").getAsString();

            superHeroes.add(new SuperHero(heroName, heroThumbnail, heroDescription));
        }

        return superHeroes;
    }

    private String sendRequest(Integer offset) {
        String result = "";
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            // URI Parameters
            String publicKey = "939b5f4fda90eb2c0045bfeb1d53fb73";
            String privateKey = "27a39d1d7cc18f5c7bfe642348ab8bad2fcb07dc";
            long timeStamp = System.currentTimeMillis();

            String stringToHash = timeStamp + privateKey + publicKey;
            String hash = DigestUtils.md5Hex(stringToHash);

            // URI
            URIBuilder builder = new URIBuilder(URL_MARVEL_CHARACTERS);
            builder.setParameter("ts", String.valueOf(timeStamp));
            builder.setParameter("apikey", publicKey);
            builder.setParameter("hash", hash);
            builder.setParameter("limit", String.valueOf(LIMIT));
            builder.setParameter("offset", String.valueOf(offset));

            // Request
            HttpGet request = new HttpGet(builder.build());
            CloseableHttpResponse response = httpClient.execute(request);

            // Response
            InputStream ips = response.getEntity().getContent();
            BufferedReader buf = new BufferedReader(new InputStreamReader(ips, StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            String s;
            while (true) {
                s = buf.readLine();
                if (s == null || s.length() == 0)
                    break;
                sb.append(s);
            }
            buf.close();
            ips.close();
            result = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
