package com.aan.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan("com.aan")
public class MarvelBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarvelBackApplication.class, args);
	}

}
