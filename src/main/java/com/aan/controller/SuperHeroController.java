package com.aan.controller;

import com.aan.dto.SuperHeroDTO;
import com.aan.service.SuperHeroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class SuperHeroController {

    @Autowired
    SuperHeroService superHeroService = new SuperHeroService();

    @GetMapping("/superheroes")
    public List<SuperHeroDTO> getSuperHeroes(@RequestParam(value = "pageNumber", defaultValue = "0") Integer pageNumber) {
        return superHeroService.getSuperHeroesPage(pageNumber);
    }
}
